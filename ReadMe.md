# BattleSharp
Консольная игра "Морской бой", реализованная на C#. 


* [Как играть] (https://gitlab.com/vestran/battlesharp/wikis/%D0%93%D0%B0%D0%B9%D0%B4-%D0%BF%D0%BE-%D0%B8%D0%B3%D1%80%D0%B5-BattleSharp)
* [Как это работает] (https://gitlab.com/vestran/battlesharp/wikis/Main())
* [Откуда мы знаем, что это работает] (https://gitlab.com/vestran/battlesharp/wikis/%D0%AE%D0%BD%D0%B8%D1%82-%D1%82%D0%B5%D1%81%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5)

Так же как и [фреймворк языка](https://github.com/microsoft/dotnet), код игры распространяется на условиях лицензии [MIT] (LICENSE). Вкратце:

1. Не удаляйте записку с лицензией
* Делайте с кодом что хотите, не противоречащее 1
* Если у вас что-то сломалось, мы не при чём

В целом это простая программка, простыми методами достигающая простых целей. 
