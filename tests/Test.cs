using NUnit.Framework;
using System;
using  Boat;


namespace ns_for_tests
{
	//функция Main() интерактивна, не тестируется (а жаль)
	
	[TestFixture()]
	public class PlayerBoardTest
	{
		//Конструктор не возвращает значения, а все переменные приватны
		//Конструктор не тестируется
		[Test()]
		public void ManTakeShotTest ()
		{
			PlayerBoard testBoard = new PlayerBoard();
			//По воде
			Assert.AreEqual(testBoard.ManTakeShot("J9"),'m');

			//Почешем спинку здоровяку
			Assert.AreEqual(testBoard.ManTakeShot("B0"),'h');

			//Прихлопнем муху
			Assert.AreEqual(testBoard.ManTakeShot("A6"),'k');

			//Подорвём крейсер в 3 хода
			Assert.AreEqual(testBoard.ManTakeShot("B2"),'h');
			Assert.AreEqual(testBoard.ManTakeShot("C2"),'h');
			Assert.AreEqual(testBoard.ManTakeShot("A2"),'k');

			//ткнём автоотмеченную клетку, получим ошибку (не повторять!)
			Assert.AreEqual(testBoard.ManTakeShot("B3"),'r');
		}

		[Test()]
		public void BotTakeShotTest ()
		{
			PlayerBoard testBoard = new PlayerBoard();
			//По воде
			Assert.AreEqual(testBoard.BotTakeShot("J9"),'m');

			//Почешем спинку здоровяку
			Assert.AreEqual(testBoard.BotTakeShot("B0"),'h');

			//Прихлопнем муху
			Assert.AreEqual(testBoard.BotTakeShot("A6"),'k');

			//Подорвём крейсер в 3 хода
			Assert.AreEqual(testBoard.BotTakeShot("B2"),'h');
			Assert.AreEqual(testBoard.BotTakeShot("C2"),'h');
			Assert.AreEqual(testBoard.BotTakeShot("A2"),'k');

			//ткнём автоотмеченную клетку, получим ошибку (не повторять!)
			Assert.AreEqual(testBoard.BotTakeShot("B3"),'r');
		}

        [Test]
        public void ManIsShotTest()
        {
            PlayerBoard testBoard = new PlayerBoard();
            testBoard.ManTakeShot("C6");	//ради эксперимента подстрелим катер

            Assert.AreEqual(testBoard.ManIsShot("J9"), false);  //чистое море
            Assert.AreEqual(testBoard.ManIsShot("A0"), false);  //целый эсминец
            Assert.AreEqual(testBoard.ManIsShot("C6"), true);   //подбитый катер
            //автометки подбитого катера
            Assert.AreEqual(testBoard.ManIsShot("B6"), true);
            Assert.AreEqual(testBoard.ManIsShot("D6"), true);
            Assert.AreEqual(testBoard.ManIsShot("B5"), true);

        }

        public void BotIsShotTest()
        {
            PlayerBoard testBoard = new PlayerBoard();
            testBoard.BotTakeShot("C6");	//ради эксперимента подстрелим катер

            Assert.AreEqual(testBoard.BotIsShot("J9"), false);  //чистое море
            Assert.AreEqual(testBoard.BotIsShot("A0"), false);  //целый эсминец
            Assert.AreEqual(testBoard.BotIsShot("C6"), true);   //подбитый катер
            //автометки подбитого катера
            Assert.AreEqual(testBoard.BotIsShot("B6"), true);
            Assert.AreEqual(testBoard.BotIsShot("D6"), true);
            Assert.AreEqual(testBoard.BotIsShot("B5"), true);

        }

        [Test]
        public void IsWinTest()
        {
            PlayerBoard winboard = new PlayerBoard();
            PlayerBoard loseboard = new PlayerBoard();
            Assert.AreEqual(winboard.IsWin(), 0);//Изначально обе партии на ничьей
            Assert.AreEqual(loseboard.IsWin(), 0);
			//побеждаем бота
            winboard.BotTakeShot("A0");
            winboard.BotTakeShot("B0");
            winboard.BotTakeShot("C0");
            winboard.BotTakeShot("D0");

            winboard.BotTakeShot("F0");
            winboard.BotTakeShot("G0");

            winboard.BotTakeShot("I0");
            winboard.BotTakeShot("J0");

            winboard.BotTakeShot("A2");
            winboard.BotTakeShot("B2");
            winboard.BotTakeShot("C2");

            winboard.BotTakeShot("E2");
            winboard.BotTakeShot("F2");
            winboard.BotTakeShot("G2");

            winboard.BotTakeShot("I2");
            winboard.BotTakeShot("J2");

            winboard.BotTakeShot("A6");
            winboard.BotTakeShot("C6");
            winboard.BotTakeShot("E6");
            winboard.BotTakeShot("G6");
            Assert.AreEqual(winboard.IsWin(), 1);//победа
			//проигрываем боту
            loseboard.ManTakeShot("A0");
            loseboard.ManTakeShot("B0");
            loseboard.ManTakeShot("C0");
            loseboard.ManTakeShot("D0");

            loseboard.ManTakeShot("F0");
            loseboard.ManTakeShot("G0");

            loseboard.ManTakeShot("I0");
            loseboard.ManTakeShot("J0");

            loseboard.ManTakeShot("A2");
            loseboard.ManTakeShot("B2");
            loseboard.ManTakeShot("C2");

            loseboard.ManTakeShot("E2");
            loseboard.ManTakeShot("F2");
            loseboard.ManTakeShot("G2");

            loseboard.ManTakeShot("I2");
            loseboard.ManTakeShot("J2");

            loseboard.ManTakeShot("A6");
            loseboard.ManTakeShot("C6");
            loseboard.ManTakeShot("E6");
            loseboard.ManTakeShot("G6");
            Assert.AreEqual(loseboard.IsWin(), -1);//поражение
        }
		//Load() работает с файлами, не тестируется
		//Save() работает с файлами, не тестируется
		//GridMenu() интерактивна, не тестируется
    }

	[TestFixture()]
	public class BotTest
	{

        [Test]
        public void BotShotTest()
        {//тестируем функцию, создающую _случайные_ выстрелы...
            string shot = Bot.RandomShot();
            Assert.GreaterOrEqual(shot[0], 'A');
            Assert.LessOrEqual(shot[0], 'J');
            Assert.GreaterOrEqual(shot[1], '0');
            Assert.LessOrEqual(shot[1], '9');
        }

	}
}


