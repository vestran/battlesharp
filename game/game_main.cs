using System;
using System.IO;
using System.Linq;


namespace Boat
//всё, что нужно для работы "Лодки"
{
	class GameFlow
	//Функции: Main()
	{
		static void Main()//ход  игры
		{
			string answ="";
			PlayerBoard tour = new PlayerBoard();
			tour.GridMenu();
			Console.WriteLine("Начало игры");
			while(tour.IsWin()==0)
			{
				Console.Write("Ваш ход: ");
				answ=Console.ReadLine();
				if(answ[0]=='!'){tour.GridMenu(); continue;}
				if(tour.BotIsShot(answ))
				{
					Console.WriteLine("Уже отмечена");
					continue;
				}
				Console.WriteLine("Игрок стреляет  по клетке {0}",answ);
				switch(tour.BotTakeShot(answ))
				{
				case 'k':
					Console.WriteLine("Убит");
					if (tour.IsWin()==1)
					{
						Console.WriteLine("Победа!");
						return;
					}
					continue;
				case 'h':
					Console.WriteLine("Ранен");
					continue;
				case 'm': 
					Console.WriteLine("Промах");
					break;
				case 'r': 
					Console.WriteLine("Ошибка в обработке выстрела");
					break;
				default: 
					Console.WriteLine("Ошибка после выстрела ");
					break;
				}
				do
				{
					answ=Bot.RandomShot();
					if (!tour.ManIsShot(answ))
					{
						Console.WriteLine("Бот стреляет по клетке {0}",answ);
						switch(tour.ManTakeShot(answ))
						{
						case 'k':
							Console.WriteLine("Убит");
							if (tour.IsWin()==1)
							{
								Console.WriteLine("Поражение");
								return;
							}
							continue;
						case 'h':
							Console.WriteLine("Ранен");
							continue;
						case 'm': 
							Console.WriteLine("Промах");
							answ="";
							break;
						case 'r': 
							Console.WriteLine("Ошибка в обработке выстрела");
							answ="";
							break;
						default: 
							Console.WriteLine("Ошибка после выстрела");
							break;
						}
					}
				}while(answ!="");
			}
			Console.WriteLine("Игра окончена, но победитель не определён");
		}
	}


	public class PlayerBoard
	//Игровое поле
	//Данные: таблицы клеток, счётчики кораблей
	//Функции: конструктор, ManTakeShot(), BotTakeShot(),
	//ManIsShot(), BotIsShot(), IsWin(), Save(), Load(),
	//GridMenu()
	{
		enum Square : byte { Sea=0, ShotSea=1, Ship=2, ShotShip=3};
		/*тип для одной клетки поля: 
		либо просто море, либо неудачный выстрел, 
		либо палуба корабля, либо простреленная палуба*/

		private Square [,] ManGrid;//поле игрока 
		private Square [,] BotGrid;//поле бота 
		private byte ManShips,BotShips;//счётчики кораблей; 0 означает поражение
		
		public PlayerBoard()
		{
			Console.WriteLine("Создание нового поля");
			ManGrid = new Square[10,10]{//стандартное размещение кораблей
				{Square.Ship, Square.Ship, Square.Ship, Square.Ship, Square.Sea, Square.Ship, Square.Ship, Square.Sea, Square.Ship, Square.Ship},
				{Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea},
				{Square.Ship, Square.Ship, Square.Ship, Square.Sea, Square.Ship, Square.Ship, Square.Ship, Square.Sea, Square.Ship, Square.Ship},
				{Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea},
				{Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea},
				{Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea},
				{Square.Ship, Square.Sea, Square.Ship, Square.Sea, Square.Ship, Square.Sea, Square.Ship, Square.Sea, Square.Sea, Square.Sea,},
				{Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea},
				{Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea},
				{Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea}
			};
			ManShips=10;
			BotGrid = new Square[10,10]{//стандартное размещение кораблей
				{Square.Ship, Square.Ship, Square.Ship, Square.Ship, Square.Sea, Square.Ship, Square.Ship, Square.Sea, Square.Ship, Square.Ship},
				{Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea},
				{Square.Ship, Square.Ship, Square.Ship, Square.Sea, Square.Ship, Square.Ship, Square.Ship, Square.Sea, Square.Ship, Square.Ship},
				{Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea},
				{Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea},
				{Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea},
				{Square.Ship, Square.Sea, Square.Ship, Square.Sea, Square.Ship, Square.Sea, Square.Ship, Square.Sea, Square.Sea, Square.Sea,},
				{Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea},
				{Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea},
				{Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea, Square.Sea}
			};
			BotShips=10;
		}


		public char ManTakeShot(string coords)
		{
			int len=0,i=0,x=coords[1]-48,y=coords[0]-65;
			bool vert=false;
			if (ManGrid[x,y]==Square.Sea)	//промах
			{
				ManGrid[x,y]=Square.ShotSea;
				return 'm';
			}
			if (ManGrid[x,y]==Square.Ship)	//попадание -> диагностика корабля
			{
				ManGrid[x,y]=Square.ShotShip;	
				if (((x>0)&&((int)(ManGrid[x-1,y])/2==1))||		//какое направление?
				    ((x<9)&&((int)(ManGrid[x+1,y])/2==1)))
				{	//if для сообщения
					vert=true;
//					Console.WriteLine("v");		//вертикально
				}
				else
				{ 
					vert=false;
//					Console.WriteLine(">");		//горизонтально
				}
				while(((vert?x:y)<=9)&&(ManGrid[x,y]==Square.ShotShip))
				{//ищем дальюю палубу
					if (vert){x++;}else{y++;}
				}
				if ((10>(vert?x:y))&&(ManGrid[x,y]==Square.Ship))
				{//если есть целая палуба, то ранен
//					Console.WriteLine("{0}{1} цела, судно на плаву",(char)(y+65),x);
					return 'h';
				}
//				Console.Write("end {0}{1}, t",(char)(y+65),x);
				if (10>(vert?x:y))//метка-граница
				{
					ManGrid[x,y]=Square.ShotSea;
					len++;
				}
				do
				{//ищем ближнюю палубу
					if (vert){x--;}else{y--;}
					len++;
				} while ((0<=(vert?x:y))&&(ManGrid[x,y]==Square.ShotShip));
				if (0>(vert?x:y))
				{
					if (vert){x++;}else{y++;}
					len--;
				}else{
					if (ManGrid[x,y]==Square.Sea){ManGrid[x,y]=Square.ShotSea;}
					if (ManGrid[x,y]==Square.Ship)
					{//если есть целая палуба, то ранен
//						Console.WriteLine("{0}{1} is whole, ship afloat",(char)(y+65),x);
						return 'h';
					}
				}
//				Console.WriteLine("start {0}{1}, len {2} ",(char)(y+65),x,len);
				if (0<((!vert)?x:y))//метки ближней границы
				{
//					Console.Write("Ближняя граница отмечается: ");
					if(!vert){x--;}else{y--;} 
					for (i=0; i<len; i++)
					{
//						Console.Write("{0}{1}, ",(char)(y+(vert?0:i)+65),x+(vert?i:0));
						ManGrid[x+(vert?i:0),y+(vert?0:i)]=Square.ShotSea;
					}
					if (!vert){x++;}else{y++;}
//					Console.WriteLine();
				}
				if (10>((!vert)?x:y))//метки дальней границы
				{
//					Console.WriteLine("Дальняя граница отмечается: ");
					if(!vert){x++;}else{y++;} 
					for (i=0; i<len; i++)
					{
//						Console.Write("{0}{1}, ",(char)(y+(vert?0:i)+65),x+(vert?i:0));
						ManGrid[x+(vert?i:0),y+(vert?0:i)]=Square.ShotSea;
					}
					if (!vert){x--;}else{y--;}
//					Console.WriteLine();
				}
				ManShips--; //убит
				return 'k';
			}//реакция на ошибки (на всякий случай)
			return 'r';
		}


		public char BotTakeShot(string coords)
		{
			int len=0,i=0,x=coords[1]-48,y=coords[0]-65;
			bool vert=false;
			if (BotGrid[x,y]==Square.Sea)	//промах
			{
				BotGrid[x,y]=Square.ShotSea;
				return 'm';
			}
			if (BotGrid[x,y]==Square.Ship)	//попадание -> диагностика корабля
			{
				BotGrid[x,y]=Square.ShotShip;	
				if (((x>0)&&((int)(BotGrid[x-1,y])/2==1))||		//какое направление?
				    ((x<9)&&((int)(BotGrid[x+1,y])/2==1)))
				{	//if для сообщения
					vert=true;
//					Console.WriteLine("v");		//вертикально
				}
				else
				{ 
					vert=false;
//					Console.WriteLine(">");		//горизонтально
				}
				while(((vert?x:y)<=9)&&(BotGrid[x,y]==Square.ShotShip))
				{//ищем дальюю палубу
					if (vert){x++;}else{y++;}
				}
				if ((10>(vert?x:y))&&(BotGrid[x,y]==Square.Ship))
				{//если есть целая палуба, то ранен
//					Console.WriteLine("{0}{1} цела, судно на плаву",(char)(y+65),x);
					return 'h';
				}
//				Console.Write("end {0}{1}, t",(char)(y+65),x);
				if (10>(vert?x:y))//метка-граница
				{
					BotGrid[x,y]=Square.ShotSea;
					len++;
				}
				do
				{//ищем ближнюю палубу
					if (vert){x--;}else{y--;}
					len++;
				} while ((0<=(vert?x:y))&&(BotGrid[x,y]==Square.ShotShip));
				if (0>(vert?x:y))
				{
					if (vert){x++;}else{y++;}
					len--;
				}else{
					if (BotGrid[x,y]==Square.Sea){BotGrid[x,y]=Square.ShotSea;}
					if (BotGrid[x,y]==Square.Ship)
					{//если есть целая палуба, то ранен
//						Console.WriteLine("{0}{1} is whole, ship afloat",(char)(y+65),x);
						return 'h';
					}
				}
//				Console.WriteLine("start {0}{1}, len {2} ",(char)(y+65),x,len);
				if (0<((!vert)?x:y))//метки ближней границы
				{
//					Console.Write("Ближняя граница отмечается: ");
					if(!vert){x--;}else{y--;} 
					for (i=0; i<len; i++)
					{
//						Console.Write("{0}{1}, ",(char)(y+(vert?0:i)+65),x+(vert?i:0));
						BotGrid[x+(vert?i:0),y+(vert?0:i)]=Square.ShotSea;
					}
					if (!vert){x++;}else{y++;}
//					Console.WriteLine();
				}
				if (10>((!vert)?x:y))//метки дальней границы
				{
//					Console.WriteLine("Дальняя граница отмечается: ");
					if(!vert){x++;}else{y++;} 
					for (i=0; i<len; i++)
					{
//						Console.Write("{0}{1}, ",(char)(y+(vert?0:i)+65),x+(vert?i:0));
						BotGrid[x+(vert?i:0),y+(vert?0:i)]=Square.ShotSea;
					}
					if (!vert){x--;}else{y--;}
//					Console.WriteLine();
				}
				BotShips--; //убит
				return 'k';
			}//реакция на ошибки (на всякий случай)
			return 'r';
		}
        public bool ManIsShot(string coords)
        {
         //Console.WriteLine("Проверка, стреляли ли раньше по клетке игрока {0}",coords);
            return !((int)(ManGrid[coords[1] - 48, coords[0] - 65]) % 2 == 0);
        }

        public bool BotIsShot(string coords)
        {
         //Console.WriteLine("Проверка, стреляли ли раньше по клетке бота {0}",coords);
            return !((int)(BotGrid[coords[1] - 48, coords[0] - 65]) % 2 == 0);
        }

        public sbyte IsWin()
        {
            //Console.WriteLine("Запрос о победе");
            if (BotShips == 0) { return 1; }
            if (ManShips == 0) { return -1; }
            return 0;
        }

        private sbyte Load(string sourceName)
		{
			sourceName = sourceName + ".txt";	
            if (File.Exists(sourceName))
            {             
                string[] lines = File.ReadAllLines(sourceName).Take(10).ToArray();
                for (int i = 0; i < 10; i++)
                {
                    byte[] row = lines[i].Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries)
                        .Select(byte.Parse).ToArray();
                    for (int j = 0; j < 10; j++)
                    {
                        ManGrid[i, j] = (Square) row[j];
                    }
                }

                ManShips = Convert.ToByte(File.ReadLines(sourceName).Skip(10).First());   
                lines = File.ReadAllLines(sourceName).Skip(11).Take(10).ToArray();
                for (int i = 0; i < 10; i++)
                {
                    byte[] row = lines[i].Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries)
                        .Select(byte.Parse).ToArray();
                    for (int j = 0; j < 10; j++)
                    {
                        BotGrid[i, j] = (Square) row[j];
                    }
                }
                BotShips = Convert.ToByte(File.ReadLines(sourceName).Skip(21).First());   
				Console.WriteLine("Данные загружены из файла: {0}", sourceName);
				/* для отладки
                for (int i = 0; i < 10; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        Console.Write(ManGrid[i,j] + " ");
                    }
                    Console.WriteLine();
                }                                
                Console.WriteLine(ManShips);
                
                for (int i = 0; i < 10; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        Console.Write(BotGrid[i,j] + " ");
                    }
                    Console.WriteLine();
                }                                
                Console.WriteLine(BotShips);
				*/
            }
            else
            {
                Console.WriteLine("Файл не существует!");
                return 1;
            }           
            return 0;
		}
		
		private sbyte Save(string placeName)
		{
			placeName += ".txt";
			if (!File.Exists(placeName))
            {
				/*для отладки
                for (int i = 0; i < 10; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        Console.Write((int) ManGrid[i,j] + " ");
                    }
                    Console.WriteLine();
                }
                Console.WriteLine("Файл не существует!");
				*/
                StreamWriter sw = new StreamWriter(placeName);
                for (int i = 0; i < 10; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        sw.Write((int) ManGrid[i, j] + " ");
                    }
                    sw.WriteLine();
                }
                sw.WriteLine(ManShips);
                
                for (int i = 0; i < 10; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        sw.Write((int) BotGrid[i, j] + " ");
                    }
                    sw.WriteLine();
                }                                
                sw.WriteLine(BotShips);
                Console.WriteLine("Данные сохранены в файл: {0}", placeName);
                sw.Close();
                return 0;
            }
            else
            {
                Console.WriteLine("Такой файл уже существует! Перезапись запрещена!");               
                return 2;
            } 
		}

        public void GridMenu()
        {
            string n = "";
            int t = 0;
            do
            {
                Console.WriteLine("Нажмите:\n1 - Для загрузки\n2 - Для сохранения\n0 - Для выхода");
                n = Console.ReadLine();

                switch (n[0])
                {
                    default:
                        Console.WriteLine("Выбирите один из предложенных пунктов меню!!!");
                        break;
                    case '1':
                        Console.WriteLine("Введите имя загружаемого файла (без расширения)");
                        n = Console.ReadLine();
                        t = Load(n);
                        if (t != 0)
                        {
                            Console.WriteLine("Загрузка не удалась!!!");
                        }

                        break;
                    case '2':
                        Console.WriteLine("Введите имя файла для сохранения (без расширения)");
                        n = Console.ReadLine();
                        t = Save(n);
                        if (t != 0)
                        {
                            Console.WriteLine("Сохранение не удалась!!!");
                        }

                        break;
                    case '0':
                        Console.WriteLine("Закрыть меню? (+/-)");
                        n = Console.ReadLine();
                        if (n[0] == '+')
                        {
                            return;
                        }

                        break;

                }
            } while (n != "0");
        }
	}

	public class Bot
	//алгоритм поведения соперника
	{
		public static string RandomShot()
		{
            Random xy = new Random();
            char [] Kk = new char [2];
            Kk[0] = (char)(xy.Next(10) + 65);
            Kk[1] = (char)(xy.Next(10) + 48);
			return new string(Kk);
		}
	}
}
